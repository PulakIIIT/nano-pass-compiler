### Nano Pass Compiler for subset of racket
- Following lecture series at IIIT-H Compilers course Spring 2022 and [Indiana University Compilers Course](https://iucompilercourse.github.io/IU-P423-P523-E313-E513-Fall-2020/).
- Following the textbook `Essentials of Compilation An Incremental Approach in Racket` by Jeremy G. Siek.
- Solving chapter 3 as of 16 Feb 2022.
- Current features of compilers:
    - Translates `Lvar` language to assembly
    - Support for register allocation using graph coloring and smart partial evaluation
